drop table if exists videos;
CREATE TABLE videos(
    id integer PRIMARY KEY autoincrement  NOT NULL,
    title text,
    youID text,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    count integer
);
CREATE VIRTUAL TABLE videos_index USING fts5(title, tokenize=porter);


CREATE TRIGGER after_videos_insert AFTER INSERT ON videos BEGIN
  INSERT INTO videos_index (
    rowid,
    title
  )
  VALUES(
    new.id,
    new.title
  );
END;

-- Trigger on UPDATE
CREATE TRIGGER after_videos_update UPDATE OF title ON videos BEGIN
  UPDATE videos_index SET title = new.title WHERE rowid = old.id;
END;

-- Trigger on DELETE
CREATE TRIGGER after_videos_delete AFTER DELETE ON videos BEGIN
    DELETE FROM videos_index WHERE rowid = old.id;
END;
