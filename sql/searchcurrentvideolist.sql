select * from videos as v join videos_index as i on i.rowid = v.id where i.title MATCH :search ORDER BY
    CASE WHEN :order = 'count' THEN v.count END DESC,
    CASE WHEN :order = 'updated_at' THEN v.updated_at END DESC
     limit 18 offset :offset;
