select * from videos ORDER BY
    CASE WHEN :order = 'count' THEN count END DESC,
    CASE WHEN :order = 'updated_at' THEN updated_at END DESC
     limit 18 offset :offset;
