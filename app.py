# -*- coding: utf-8 -*-
import os
from flask import Flask, abort
from flask import render_template, send_from_directory
from flask import redirect, url_for
from flask import request, make_response, session
import requests, split, json, urllib
import sqlite3 as sql
from bs4 import BeautifulSoup
import re
from datetime import datetime, timedelta

import random

import sys
reload(sys)
sys.setdefaultencoding('utf-8')



app = Flask(__name__)

BASE_DIR = app.root_path
# Configure database
app.config.update(dict(
    DATABASE=os.path.join(app.root_path, 'flaskr.db'),
))

def request_db(fd, params=()):
    con = sql.connect(app.config['DATABASE'])
    con.text_factory = str
    con.row_factory = lambda c, r: dict([(col[0], r[idx]) for idx, col in enumerate(c.description)])
    #sql.Row
    cur = con.cursor()
    with open(fd) as fd:
        try:
            cur.execute(fd.read(), params)
            return cur.fetchall()
        except Exception as e:
            con.rollback()
            raise
    con.close()

def write_db(fd, params=None):
    con = sql.connect(app.config['DATABASE'])
    con.text_factory = str
    con.row_factory = sql.Row
    cur = con.cursor()
    with open(fd) as fd:
        try:
            cur.execute(fd.read(), params)
            con.commit()
        except Exception as e:
            con.rollback()
            raise
    con.close()

# Custom static data
@app.route('/statics/<path:filename>')
def custom_static(filename):
    return send_from_directory(os.path.join(BASE_DIR, "statics"), filename)

@app.route('/')
def index():
    search= request.args.get('search', '')
    orderBy= request.args.get('orderBy', 'count')
    page= int(request.args.get('page', 0))
    offset = page*18
    videos = request_db('sql/videolist.sql')
    pageNumber = int(len(videos)/18)
    if search=='':
        currentvideos = request_db('sql/currentvideolist.sql', {'order':orderBy, 'page': page, 'offset':offset})
    else:
        currentvideos = request_db('sql/searchcurrentvideolist.sql', {'order':orderBy, 'search': search, 'page': page, 'offset':offset})
    alea = get_random_page_and_video(videos)
    return render_template('index.html',  videos=currentvideos, random_page=alea[0], random_video=alea[1], page=int(page), search=search, pageNumber=pageNumber)


@app.route("/search/<search>")
def search(orderBy='updated_at'):
    videos = request_db('sql/searchvideolist.sql', {'search':search})
    return render_template('index.html', videos=videos)

@app.route("/addorincvideo/<videoID>")
def addorincvideo(videoID):
    video=request_db('sql/getvideobyid.sql', (videoID,))
    if len(video)==0:
        title = get_video_title(videoID)
        write_db('sql/addvideo.sql', (videoID, title, 1))
    else:
        write_db('sql/incvideo.sql', (video[0]['count']+1, videoID))
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

def get_video_title(videoID):
    try:
        url = "https://www.youtube.com/watch?v=" + videoID
        # getting the request from url
        title = os.popen('youtube-dl -e '+url).read()
        return title
    except:
        return "Handle missing video here"

def get_random_page_and_video(videos):
    random_page = random.randint(1, 1+len(videos)/18)-1
    random_video = videos[random.randint(1, len(videos))-1]['youID']
    return random_page, random_video

if __name__ == "__main__":
    app.run(host="localhost", port=5033)
#    app.run(host="10.0.160.54")
